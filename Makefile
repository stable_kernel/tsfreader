C=c++ -std=c++11

all: a.out

a: all run

a.out: main.o
	$(C) main.cpp

main.o: main.cpp
	$(C) -c main.cpp

clean:
	rm a.out main.o

run:
	./a.out in1 in2

#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>
#include <vector>
#include <stdlib.h>

#define DP 0
#define N 16

using namespace std;

#define C_STRING_LENGHT  1024

using namespace std;
//	...0
//	0000


int count_in_line( char *s, char sep, char **words) {
	int count=0;
	char *begin = s;
	for(char *p=s; *p; ++p ){
		if(*p == sep){
			printf ("sep fonuded in %d pos\n", (int)(p-s));
			words[count++] = begin;
			begin = p+1;
			*p = 0;
		}
	}
	words[count++] = begin;
	return count;
}

class TSFReader {
private:
	FILE *f;
	int length, count;
	size_t linecap;
	char *words[N], *cline=0;
public:
	TSFReader(const char *fname){
		if(!(f=fopen(fname, "r"))) {
			printf("Cannot open file <%s>\n", fname);
			exit(1);
		}
	}

	int read(char ***wordsr){
		if((length = getline(&cline, &linecap, f)) <=0)
			return 0;
		int count = count_in_line (cline, '.', words);
		if(DP){
			printf("length = %d\nline = <%s>\ncapacity = %d\n", length, cline, (int)linecap);
			printf("count = %d\n", count);
			for(int i=0; i<count; i++) {
				printf("word = %s\n", words[i]);
			}
			printf("\n");
		}
		*wordsr = words;

		return count;
	}
};

int main (){
	TSFReader reader("in1");

	char **words;
	int count;
	while ((count = reader.read(&words))> 0 ){
		for(int i=0; i<count; i++){
			printf("%s_", words[i]);
		}
		printf("\n");
	}

	return 0;
}




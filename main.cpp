#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#define OK 1
#define isok(x) ((x) == OK)
#define DP 0
#define N 16
#define C_STRING_LENGHT  1024

using namespace std;
//	...0
//	0000

class TSFReader {
	private:
		FILE *f;
		
		/*  this is for getline */
		char *cline = 0;
		int length, count;
		size_t linecap;
		const char sep = '.';


	public:
		string file_name;
		char  *data;
		int key;

		TSFReader(){};

		TSFReader(const TSFReader &copy){
			cline=copy.cline;
			length=copy.length;
			key=copy.key;
			linecap=copy.linecap;
		}


		TSFReader(const char *fname){
			file_name = fname;
			if(!(f=fopen(fname, "r"))) {
				printf("Cannot open file <%s>\n", fname);
				exit(1);
			}
		}

		int read(){
			if((length = getline(&cline, &linecap, f)) <=0)
				return -1;
			if(cline[length-1]=='\n')
				cline[length-1]=0;

//			key = cline;
			key = atoi(cline);
			for(char *p = cline; *p != 0; ++p) {
				if ( *p == sep ){
					*p=0;
					data = p+1;
					break;
				}
			}
			return OK;
		}

		int get_count() {
			return count;
		}
};

/*
 *
 *  DefinedKeys[]->minKey->out<<minKey<<_for(inputs[]_Key=minKey && move)
 *
 *  inputs[]
 *
 *
 *
 *
 *
 *
 */

template<class T>
class MM {
	vector<T> v;
	MM();
	void push(T e){
	};
	T get() {
		return v.pop();
	}
};

int main (int argc, char **argv) {
	vector<TSFReader>  v;
//	TSFReader r1("in1");
//	TSFReader r2("in2");

	for(size_t i = 1; i<argc; ++i){
		cout << argv[i] << endl;
		v.push_back(TSFReader(argv[i]));
	}
	/*
	while(isok(r1.read())){
		cout << "<" << r1.key << '>' << endl;
		cout << '<' << r1.data <<  '>' << endl;
	}
	cout << "END_read" << endl;
*/
	return 0;
}




